# Datahub Event Streams Ingestion

Takes topics from Kafka, checks it against [streamconfigs](https://meta.wikimedia.org/w/api.php?action=streamconfigs&format=json)
and then add the topics with schema, description, and normalized path to Datahub while
at the same time adding the streams to a separate `Event Streams` platform and connecting it as an upstream lineage to the topics.

## Local Development

> Prerequisites: Python 3, Docker Desktop, Minikube

1. Download Kafka (we'll need to use `bin/kafka-topics.sh`)

2. Install Datahub (preferably in a virtual environment)

```bash
pip install 'acryl-datahub[datahub-rest]'
pip install 'acryl-datahub[kafka]'
```

3. Run

```bash
datahub docker quickstart
```

Clone `deployment-charts`

4. Run `kafka-dev` chart and port forward
(This is assuming Mac; see the chart's README)

```bash
minikube start
helm install kafka ./charts/kafka-dev

minikube ssh
sudo ip link set docker0 promisc on

helm install kafka ./charts/kafka-dev
kubectl port-forward svc/kafka-external 30092
```

5. Put some topics into kafka

```bash
bin/kafka-topics.sh --create --bootstrap-server 127.0.0.1:30092 --topic eqiad.mediawiki.revision-create
bin/kafka-topics.sh --create --bootstrap-server 127.0.0.1:30092 --topic codfw.mediawiki.revision-create
```

6. Run

```bash
datahub ingest -c ./kafka.yaml --dry-run
```

If on a stat server and connecting to the prod instance, you need the ssl cert.
```bash
REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt datahub ingest -c ./kafka.yaml --dry-run
```

7. Remove `--dry-run` when you want to insert into Datahub.
Go to `http://localhost:9002/` to see frontend.

8. Destroy everything

```bash
datahub docker nuke
```